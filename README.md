
# Quad V3
![v3 Robot](https://gitlab.com/lococorp-opensource/quad-v3/-/raw/main/Pictures/quad-v3.jpg)  
Quad v3 - Hardware and Firmware files

## Getting started

We will need three parts to make this robot alive:
- [ ] 3D Parts
- [ ] Electronics
- [ ] Firmware

## 3D Parts
We  need to print the parts for assemblying the robot. There are some mounting hardware necesary, but worry not, it just a few screws that you probably have laying aroung.  

**3D Print**   
[BATTERY SHIELD](https://gitlab.com/lococorp-opensource/quad-v3/-/blob/main/3D%20Parts/BATTERY_SHIELD.stl)  x1  
[BODY](https://gitlab.com/lococorp-opensource/quad-v3/-/blob/main/3D%20Parts/BODY.stl)  x1  
[FACE](https://gitlab.com/lococorp-opensource/quad-v3/-/blob/main/3D%20Parts/FACE.stl)  x1  
[FLOOR](https://gitlab.com/lococorp-opensource/quad-v3/-/blob/main/3D%20Parts/FLOOR.stl)    x1  
[FOOT](https://gitlab.com/lococorp-opensource/quad-v3/-/blob/main/3D%20Parts/FOOT.stl)  x4  
[GRAP A](https://gitlab.com/lococorp-opensource/quad-v3/-/blob/main/3D%20Parts/GRAP_A.stl)  x4  
[GRAP B](https://gitlab.com/lococorp-opensource/quad-v3/-/blob/main/3D%20Parts/GRAP_B.stl)  x4  
[HOOD](https://gitlab.com/lococorp-opensource/quad-v3/-/blob/main/3D%20Parts/HOOD.stl)  x1  
[LINK](https://gitlab.com/lococorp-opensource/quad-v3/-/blob/main/3D%20Parts/LINK.stl)  x4  

**Mounting hardware**  
[D912 M3 8mm](https://es.aliexpress.com/item/1005002282969565.html) x14  
[M3 Nut](https://es.aliexpress.com/item/1005002925742683.html) x8  
[M3 20mm Spacer](https://es.aliexpress.com/item/32898985398.html) x4  



## Electronics  
Here you must make a choice. Either you order our PCB that allows you to put together several modules or you glue them to the 3D parts. Both uses the same Bill of Materials.

**Electronic Hardware**  
[DCDC Boost MT3608](https://es.aliexpress.com/item/32950228668.html) x1  
[Lithium 2S Charger](https://es.aliexpress.com/item/32911247990.html) x1  
[DCDC Buck XL4005](https://es.aliexpress.com/item/33053215209.html) x1  
[PWM Driver PCA9685](https://es.aliexpress.com/item/4000468996665.html) x1  
[Wemos D1 ESP32](https://es.aliexpress.com/item/32834982479.html) x1  
[Metal Gear 9g Servo](https://es.aliexpress.com/item/1005002219839349.html) x8  

**PCB Assembly**  
![PCB Robot](https://gitlab.com/lococorp-opensource/quad-v3/-/raw/main/Pictures/pcb-robot.jpg)

**Module Assembly 1**    
![Module Breadboard](https://gitlab.com/lococorp-opensource/quad-v3/-/raw/main/Pictures/modules-bb-robot.jpg)

**Module Assembly 2**    
![Module Protoboard](https://gitlab.com/lococorp-opensource/quad-v3/-/raw/main/Pictures/modules-proto-robot.jpg)

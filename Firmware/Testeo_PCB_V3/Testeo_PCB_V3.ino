//******************************\\
 //Programa para testeo de PCBs V3*\\
///********************************\\\


//En la primera ejecución se hacen 5 parpadeos cada vezs mas largos
//En todos los ciclos se hace un barrido individual a 0º y 180º
//Despues pone todos a la vez a 90º

//Librerias
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

//********************************
//*Defines usuario**
//********************************

#define SERVO_MAX 8 //Numero de servos 8 u 12
//Conectores servos empleados (segun la serigrafia de la pcb)
#define CONECTORES             \
  {                            \
    1, 3, 5, 6, 11, 12, 14, 16 \
  }

//Habilitar barrido individual o prueba de carga
#define SINGLE
#define CALIBRATE
//#define LOAD_TEST
//Posiciones Caracteristicas
#define POS_0 103   // ancho de pulso en cuentas para 0º, 0.5ms
#define POS_90 307  //Ancho de pulso para 90, 1.5ms
#define POS_180 512 // ancho de pulso en cuentas para 180°, 2.5ms
//#define POS_45 = 205;//Ancho de pulso para 45º, 1ms
//#define POS_135 = 481;//2ms

//********************************
//*Variables globales necesarias**
//********************************
Adafruit_PWMServoDriver servos = Adafruit_PWMServoDriver(0x40);

//**********
//*Programa*
//**********

void setup()
{
  //Pin del LED como salida
  pinMode(D4, OUTPUT);
  //Pin para habilitar el driver de servos
  pinMode(D5, OUTPUT);
  //Inicio comunicaciones
  servos.begin();
  servos.setPWMFreq(50); //Frecuecia PWM de 50Hz o T=20ms
  Serial.begin(115200);

  //Cinco parpadeos para comprobar LED, cada uno más largo que el anterior
  for (int i = 1; i < 6; i++)
  {
    digitalWrite(D4, HIGH);
    delay(i * 100);
    digitalWrite(D4, LOW);
    delay(i * 100);
  }
}

void loop()
{
    static int conectores[] = {1, 3, 5, 6, 11, 12, 14, 16, 2, 8, 9, 15 };
#ifdef SINGLE
  //Barrido individual
  for (int i = 0; i < SERVO_MAX; i++)
  {
    int servo = conectores[i] - 1;

    servos.setPWM(servo, 0, POS_0);
    delay(100);
    digitalWrite(D5, LOW);  //Serv ON
    digitalWrite(D4, HIGH); //LED ON
    delay(1000);

    digitalWrite(D5, HIGH); //Servos OFF
    digitalWrite(D4, LOW);  //LED OFF
    servos.setPWM(servo, 0, POS_180);
    delay(100);
    digitalWrite(D5, LOW);  //Serv ON
    digitalWrite(D4, HIGH); //LED ON
    delay(1000);
    digitalWrite(D5, HIGH); //Servos OFF
    digitalWrite(D4, LOW);  //LED OFF
    servos.setPWM(servo, 0, 4096);
    delay(100);
  }
#endif
#ifdef CALIBRATE
  //Fija a 90 grados
  digitalWrite(D5, LOW);  //Serv ON
  digitalWrite(D4, HIGH); //LED ON
  for (int i = 0; i < SERVO_MAX; i++)
  {
    int servo = conectores[i] - 1;

    servos.setPWM(servo, 0, POS_90);

    delay(1000);
  }
  digitalWrite(D5, HIGH); //Servos OFF
  digitalWrite(D4, LOW);  //LED OFF
  delay(100);
  while (1)
  {
  }
#endif
#ifdef LOAD_TEST

  for (int i = 0; i < SERVO_MAX; i++)
  {
    int servo = 0;
    for (int j = 0; j <= i; j++)
    {
      servo = conectores[j] - 1;
      servos.setPWM(servo, 0, POS_0);
    }
    delay(100);
    digitalWrite(D5, LOW);  //Serv ON
    digitalWrite(D4, HIGH); //LED ON
    delay(1000);

    digitalWrite(D5, HIGH); //Servos OFF
    digitalWrite(D4, LOW);  //LED OFF
    for (int j = 0; j <= i; j++)
    {
      servo = conectores[j] - 1;
      servos.setPWM(servo, 0, POS_180);
    }
    delay(100);
    digitalWrite(D5, LOW);  //Serv ON
    digitalWrite(D4, HIGH); //LED ON
    delay(1000);
    digitalWrite(D5, HIGH); //Servos OFF
    digitalWrite(D4, LOW);  //LED OFF
  }
#endif
}

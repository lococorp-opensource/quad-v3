/*	LoCoMotion
*	Function: This program allows the robot to move on a Finite State Machine.
*	It sequentially calculates the next movement and then it executes it.
*	The program is simple so you can add your own movements to the chain.
*	Author: Arturo Hermosa
*	Date: November 2021
*/


//Includes
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>


#define LOCOQUAD_SCHOOL

//Common inline functions
#define DUTY(X) map(X,0,180,POS_INI_PWM,POS_FIN_PWM)

//Operation values
#define DELAY_SET 1000/FREQ             //time to wait between orders [1000 @ Arturo]
#define MOV_DELTA DELAY_SET/ANG_SPEED   //how many degrees each order
#define N_SERVOS LEGS*JOINTS            //number of servos
#define POS_INI_PWM 103 // 2.5%
#define POS_FIN_PWM 512 // 12.5%
//Limit position
#define POS_INI_DEG 90-ANG_RANGE
#define POS_FIN_DEG 90+ANG_RANGE

/*We will create one block for each kind of robot. Depending on the number of
* parameters we might have to define additional ones on the main program.
* For example, we expect locoquad school to have all parameters defined,
* but what about a development 4 leg 2 joint general robot?
*/
#ifdef LOCOQUAD_SCHOOL      //name of this set of parameters
#define LEGS 4
#define JOINTS 2

//SERVO ARRAY ORDER --> FSL, FLL, FSR, FLR, BSR, BLR, BSL, BLL
//SERVO ARRAY CLIBRATION VALUES --> 90,90,90,90,90,90,90,90
//SERVO ARRAY LOW VALUES --> 40,135,140,45,40,135,140,45
//SERVO ARRAY HIGH VALUES --> 140,0,40,180,140,0,40,180

//Servo connector
#define FSL 2
#define FSR 13
#define BSL 5
#define BSR 10
#define FLL 0
#define FLR 15
#define BLL 4
#define BLR 11

//order for array sequences
#define SERVO_ARRAY {FSL, FLL, FSR, FLR, BSR, BLR, BSL, BLL}

//Settings for your specific robot
#define FREQ 50 //hz
#define ANG_SPEED 1.66 //ms
#define ANG_RANGE 180/2 //degrees half range



//FLAT POSES
uint8_t flatH[] = {135,90,45,90,135,90,45,90};
uint8_t flatI[] = {45,90,135,90,45,90,135,90};
uint8_t flatL[] = {45,90,135,90,135,90,45,90};
uint8_t flatT[] = {135,90,45,90,45,90,135,90};
uint8_t flatX[] = {90,90,90,90,90,90,90,90};
uint8_t flatA[] = {45,90,135,90,90,90,90,90};
//STAND POSES
uint8_t standX[] = {90,0,90,180,90,0,90,180};
uint8_t standH[] = {135,0,45,180,135,0,45,180};
uint8_t standI[] = {45,0,135,180,45,0,135,180};
uint8_t standT[] = {135,0,45,180,45,0,135,180};
uint8_t standL[] = {45,0,135,180,135,0,45,180};
uint8_t standA[] = {45,0,135,180,90,0,90,180};

//HELLO POSES
//uint8_t hello1[] = standX;
//uint8_t hello2[] = standL;

uint8_t hello2[] = {45,0,135,10,90,0,90,180};
uint8_t hello3[] = {45,0,135,10,90,0,90,180};
uint8_t hello4[] = {45,0,90,10,90,0,90,180};

//SWING POSES
uint8_t swing1[] = {135,0,45,90,135,90,45,180};
uint8_t swing2[] = {135,90,45,180,135,0,45,90};

//SHOW OFF POSES
uint8_t doggy1[] = {45,90,135,90,135,0,45,180};
uint8_t picture1[] = {135,0,45,180,45,90,135,90};

//DANCING ROBOT
//uint8_t* movement[] = {flatX,flatL,standL,standX};
uint8_t* hello[] = {flatA, standA, hello4,hello3,hello4,hello3,hello4};
uint8_t* idle[] = {standA,standX};
uint8_t* swing[] = {flatX,flatH,standH,swing1,swing2,swing1,swing2,swing1,swing2,standH};
uint8_t* doggy[] = {standX,doggy1};
uint8_t* picture[] = {flatX,picture1};
uint8_t* pushup[] = {flatT,picture1,flatT,picture1};

//FORWARD
const int forward = 4;
int forward1[] = {0,0,0,0,0,0,0,0};
int forward2[] = {0,0,0,0,0,0,0,0};
int forward3[] = {0,0,0,0,0,0,0,0};
int forward4[] = {0,0,0,0,0,0,0,0};

//BACKWARD
const int backward = 4;
int backward1[] = {0,0,0,0,0,0,0,0};
int backward2[] = {0,0,0,0,0,0,0,0};
int backward3[] = {0,0,0,0,0,0,0,0};
int backward4[] = {0,0,0,0,0,0,0,0};

//FLAT
const int flat = 1;
int flat1[] = {90,90,90,90,90,90,90,90};

//STAND
const int stand = 1;
int stand1[] = {90,0,90,180,90,0,90,180};
#endif


//Shady libraries requirements
Adafruit_PWMServoDriver servos = Adafruit_PWMServoDriver(0x40);

//We need the array in memory so we can iterate it
const uint8_t servoArray[] = SERVO_ARRAY;

//State Machine Variables
//Signals movement scheduler
uint8_t nextMov = 0;
//Signals movement runtime
uint8_t currPos[N_SERVOS]; 
uint8_t nextPos[N_SERVOS]; 
uint8_t loadNextPos = 1;

//Sets flat pose and fills position arrays
void initPose (){
    uint8_t i;
    for(i=0;i<N_SERVOS;i++){
        nextPos[i]=90;
        currPos[i]=90;
        servos.setPWM(servoArray[i],0,DUTY(90));
        delay(ANG_RANGE*ANG_SPEED);
    }
}


//Loads positions into memory
void loadPos(uint8_t* posArr[], uint8_t lnt, uint8_t rpt){
    static uint8_t i = 0;
    Serial.print("Loading pos num ");
    Serial.println(i);
    //check if we iterated all elements in the array
    if(i<lnt){                       
        memcpy(&nextPos,posArr[i],N_SERVOS);    //copy values to position array
        i++;
        loadNextPos = 0;                        //acknowledges the loading
    }else{
            nextMov = nextMov*rpt;              //if repeat, reload the move, else go to idle
            i=0;
    }
}

//Schedules next moves according to info received from user or internal runtime
void movementScheduler(){    
    static uint8_t currMov = 0;
    uint8_t sizeArr = 0; 
    uint8_t sizeEle = 0;
    //if we loaded the final move we move onto the next one or received stop order and kill runtime
    if(currMov==0 || nextMov ==0){
        currMov = nextMov;
        Serial.print("set movement num ");
        Serial.println(currMov);
    }
    if(loadNextPos){
        //EXAMPLE, FILL WITH POSIBLE CASES. e.g. we received through bluetooth a 2 and set it as nextMov
        switch(currMov){
            case 0:
                loadNextPos=0;
            break;
            case 1:
                Serial.println("loading movement num 1");
                sizeArr = sizeof(hello); 
                sizeEle = sizeof(hello[0]);
                loadPos(hello,sizeArr/sizeEle, 0);
            break;
            case 2:
            Serial.println("loading movement num 2");
                loadPos(idle,2, 0);
            break;
            case 3:
                Serial.println("loading movement num 3");
                sizeArr = sizeof(swing); 
                sizeEle = sizeof(swing[0]);
                loadPos(swing,sizeArr/sizeEle, 0);
            break;
            case 4:
                Serial.println("loading movement num 4");
                sizeArr = sizeof(doggy); 
                sizeEle = sizeof(doggy[0]);
                loadPos(doggy,sizeArr/sizeEle, 0);
            break;
            case 5:
                Serial.println("loading movement num 5");
                sizeArr = sizeof(picture); 
                sizeEle = sizeof(picture[0]);
                loadPos(picture,sizeArr/sizeEle, 0);
            break;
            case 6:
                Serial.println("loading movement num 6");
                sizeArr = sizeof(pushup); 
                sizeEle = sizeof(pushup[0]);
                loadPos(pushup,sizeArr/sizeEle, 0);
            break;
        }
    }
}

//Runtime running on a Timer. For simplicity now running on a blocking manner with delays
void movementRuntime(){
    //If i'm already on destination give me next position
    if(!memcmp(&currPos,&nextPos,sizeof(currPos))){ //true on equal
        loadNextPos = 1;
    }else{
        int i;
        for(i=0;i<N_SERVOS;i++){
            if(currPos[i]!=nextPos[i]){ //if servo is already set, do nothing
                int8_t deltaAng = currPos[i]-nextPos[i];
                if(abs(deltaAng)>=MOV_DELTA){ //movement greater than time between calls to this func
                    if(deltaAng < 0){
                        currPos[i] = currPos[i] + MOV_DELTA;
                    }else{
                        currPos[i] = currPos[i] - MOV_DELTA;
                    }
                }else{  //we reach destination next call
                    currPos[i] = nextPos[i];
                }
                servos.setPWM(servoArray[i],0,DUTY(nextPos[i]));            
            }
        }
        delay(DELAY_SET); //MAGICAL DELAY THAT SHOULD BE REPLACED WITH A TIMER
        
    }
}


void setup() {
    pinMode(D5,OUTPUT); //Servo enable
    pinMode(LED_BUILTIN,OUTPUT); //LED ESP32

    servos.begin();  
    servos.setPWMFreq(FREQ); //Frecuecia PWM de 50Hz o T=20ms    
   
    digitalWrite(D5, LOW);
    digitalWrite(LED_BUILTIN, HIGH);

    Serial.begin(115200);
    Serial.println("Hello LoCo");
    
    delay(1000);
    initPose();
    delay(1000);
    //TEST VALUE FOR DEMONSTRATION PURPOSES
    nextMov = 3;
   }

  void loop() {
    static uint8_t count = 0;
    static uint8_t endWave = 0;

    
    digitalWrite(LED_BUILTIN, (count&0x1));
    
    movementScheduler();
    
    // if(nextMov == 0 && loadNextPos  == 0 && endWave == 0) {
    //     Serial.println("We load thus we exist");
    //     endWave = 1;
    //     nextMov = 2;
    // }

    if(nextMov == 0 && loadNextPos  == 0) {
        Serial.println("We load thus we exist");
        endWave++;
        nextMov += endWave;
        if(nextMov>6){
            nextMov = 0;
        }
    }

    movementRuntime(); //we should run this on a timer
      
     count++; 
  }

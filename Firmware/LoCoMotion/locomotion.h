
/** @file movement.h
 * 
 * @brief Servo movement strategy and structure. 
 *
 * @par       
 * COPYRIGHT NOTICE: (c) 2021 LoCo CORP.  All rights reserved.
 */ 

#ifndef LOCOMOTION_H
#define LOCOMOTION_H


//Common inline functions
#define DUTY(X) map(X,0,180,POS_INI_PWM,POS_FIN_PWM)

//Operation values
#define DELAY_SET 1000/FREQ //
#define MOV_DELTA DELAY_SET/ANG_SPEED 
#define N_SERVOS LEGS*JOINTS
#define POS_INI_PWM 103 // 2.5%
#define POS_FIN_PWM 512 // 12.5%
//Limit position
#define POS_INI_DEG 90-ANG_RANGE
#define POS_FIN_DEG 90+ANG_RANGE

/*We will create one block for each kind of robot. Depending on the number of
* parameters we might have to define additional ones on the main program.
* For example, we expect locoquad school to have all parameters defined,
* but what about a development 4 leg 2 joint general robot?
*/
#ifdef LOCOQUAD_SCHOOL
#define LEGS 4
#define JOINTS 2

//SERVO ARRAY ORDER --> FSL, FLL, FSR, FLR, BSR, BLR, BSL, BLL
//SERVO ARRAY CLIBRATION VALUES --> 90,90,90,90,90,90,90,90
//SERVO ARRAY LOW VALUES --> 40,135,140,45,40,135,140,45
//SERVO ARRAY HIGH VALUES --> 140,0,40,180,140,0,40,180

//Servo connector
#define FSL 2
#define FSR 13
#define BSL 5
#define BSR 10
#define FLL 0
#define FLR 15
#define BLL 4
#define BLR 11

#define SERVO_ARRAY {FSR,FSL,BSR,BSL,FLR,FLL,BLR,BLL}

//Settings for your specific robot
#define FREQ 333 //hz
#define ANG_SPEED 2 //ms
#define ANG_RANGE 180/2 //degrees half range



//RANDOM MANU SEQUENCE


uint8_t flatH[] = {135,90,45,90,135,90,45,90};
uint8_t flatI[] = {45,90,135,90,45,90,135,90};
uint8_t flatL[] = {45,90,135,90,135,90,45,90};
uint8_t flatT[] = {135,90,45,90,45,90,135,90};
uint8_t flatX[] = {90,90,90,90,90,90,90,90};
uint8_t standX[] = {90,0,90,180,90,0,90,180};
uint8_t standH[] = {135,0,45,180,135,0,45,180};
uint8_t standI[] = {45,0,135,180,45,0,135,180};
uint8_t standT[] = {135,0,45,180,45,0,135,180};
uint8_t standL[] = {45,0,135,180,135,0,45,180};

uint8_t* movement[] = {flatX,flatL,standL,standX};


//

//FORWARD
const int forward = 4;
int forward1[] = {0,0,0,0,0,0,0,0};
int forward2[] = {0,0,0,0,0,0,0,0};
int forward3[] = {0,0,0,0,0,0,0,0};
int forward4[] = {0,0,0,0,0,0,0,0};

//BACKWARD
const int backward = 4;
int backward1[] = {0,0,0,0,0,0,0,0};
int backward2[] = {0,0,0,0,0,0,0,0};
int backward3[] = {0,0,0,0,0,0,0,0};
int backward4[] = {0,0,0,0,0,0,0,0};

//FLAT
const int flat = 1;
int flat1[] = {90,90,90,90,90,90,90,90};

//STAND
const int stand = 1;
int stand1[] = {90,0,90,180,90,0,90,180};
#endif

#endif /* LOCOMOTION_H */

/*** end of file ***/
